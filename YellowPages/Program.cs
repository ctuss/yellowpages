﻿using System;
using System.Collections.Generic;

namespace YellowPages {
    class Program {
        static void Main(string[] args) {
            bool doContinue = true;
            Person p;
            string input;
            string inputFn, inputLn;
            int decision;

            List<Person> personList = new List<Person>();
            personList.Add(new Person("Christopher", "Toussi", 94082639, "Løvstakklien 10"));
            personList.Add(new Person("Ola", "Nordmann", 22225555, "Norgesveien 1337"));
            personList.Add(new Person("Kari", "Nordmann", 55552222, "Norgesveien 69"));
            personList.Add(new Person("Muhammad", "Ali", 123456789, "Bakerstreet 34"));
            Console.WriteLine("-------------------------");
            Console.WriteLine("-------Yellow Pages------");

            do
            {
                Console.WriteLine("-------------------------");
                Console.WriteLine("Do you wish to make a search? Press 1\n" +
                    "Do you wish to add a new person? Press 2\n" +
                    "Do you wish to make change to a person? Press 3\n" +
                    "To close application press 0");
                input = Console.ReadLine();
                decision = Convert.ToInt32(input);
                Console.WriteLine("-------------------------");
                switch (decision)
                {
                    case 1: 
                        Console.WriteLine("Please enter search term: ");
                        input = Console.ReadLine();
                        SearchForPerson(input, personList);
                        break;
                    case 2: 
                        Console.WriteLine("Please enter first name: ");
                        inputFn = Console.ReadLine();
                        Console.WriteLine("Please enter last name: ");
                        inputLn = Console.ReadLine();
                        AddPersonToList(personList, inputFn, inputLn);
                        break;
                    case 3:
                        EditContactPerson(personList);
                        break;
                    default:
                        doContinue = false;
                        break;
                }

            } while (doContinue);
        }

        public static void SearchForPerson(string searchInput, List<Person> list) {
            foreach(Person p in list)
            {
                if(p.ToString().Contains(searchInput))
                {
                    Console.WriteLine(p.ToString());
                }
            }
        }
        public static void AddPersonToList(List<Person> list, string fn, string ln)
        {
            string input, inputPhone;
            int number, pn;

            Console.WriteLine("-------------------------");
            Console.WriteLine("Do you wish to add phone number and address? Press 1" +
                "\nOnly phone number? Press 2" +
                "\nOnly address? Press 3" +
                "\nExit? Press 0");
            input = Console.ReadLine();
            number = Convert.ToInt32(input);

            switch(number)
            {
                case 1:
                    Console.WriteLine("Provide phone number: ");
                    inputPhone = Console.ReadLine();
                    pn = Convert.ToInt32(inputPhone);
                    Console.WriteLine("Provide address: ");
                    input = Console.ReadLine();
                    list.Add(new Person(fn, ln, pn, input));
                    break;
                case 2:
                    Console.WriteLine("Provide phone number: ");
                    inputPhone = Console.ReadLine();
                    pn = Convert.ToInt32(inputPhone);
                    list.Add(new Person(fn, ln, pn));
                    break;
                case 3:
                    Console.WriteLine("Provide address: ");
                    input = Console.ReadLine();
                    list.Add(new Person(fn, ln, input));
                    break;
                default:
                    break;
            }

        }
        public static void EditContactPerson(List<Person> list)
        {
            string input;
            int contactId, decision, phone;
            int i = 0;
            bool doContinue = true;
            foreach(Person p in list)
            {
                Console.WriteLine($"Id: {i++} | " + p.ToString());
            }

            Console.WriteLine("Select ID on contact you wish to change: ");
            input = Console.ReadLine();
            contactId = Convert.ToInt32(input);

            

            while (doContinue)
            {
                Console.WriteLine("Change last name: Press 1" +
                "\nChange phone number: Press 2" +
                "\nChange address: Press 3" +
                "\nExit: Press 0");
                input = Console.ReadLine();
                decision = Convert.ToInt32(input);

                switch (decision)
                {
                    case 1:
                        Console.WriteLine("Last name:");
                        input = Console.ReadLine();
                        list[contactId].LastName = input;
                        break;
                    case 2:
                        Console.WriteLine("Phone number:");
                        input = Console.ReadLine();
                        phone = Convert.ToInt32(input);
                        list[contactId].PhoneNumber = phone;
                        break;
                    case 3:
                        Console.WriteLine("Address:");
                        input = Console.ReadLine();
                        list[contactId].Address = input;
                        break;
                    default:
                        doContinue = false;
                        break;
                }
            }
        }
    }
}
