﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YellowPages
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public string Address { get; set; }

        public Person(string fn, string ln)
        {
            FirstName = fn;
            LastName = ln;
        }

        public Person(string fn, string ln, int pn)
        {
            FirstName = fn;
            LastName = ln;
            PhoneNumber = pn;
        }

        public Person(string fn, string ln, string a)
        {
            FirstName = fn;
            LastName = ln;
            Address = a;
        }

        public Person(string fn, string ln, int pn, string a)
        {
            FirstName = fn;
            LastName = ln;
            PhoneNumber = pn;
            Address = a;
        }

        public override string ToString()
        {
            return $"First name: {FirstName} - Last name: {LastName} - Phone number: {PhoneNumber} - Address: {Address}";
        }
    }
}
